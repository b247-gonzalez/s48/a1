/*
Thor: Love and Thunder
2022 | Action/Adventure

Doctor Strange in the Multiverse of Madness
2022 | Action/Adventure

Minions: The Rise of Gru
2022 | Comedy/Adventure

Black Panther: Wakanda Forever
2022 | Action Adventure
*/

// Mock DB
let posts = [];

// POST Id
let count = 1;

// Add POST data
document.querySelector("#form-add-post").addEventListener('submit', (e) => {
    e.preventDefault();

    posts.push({
        id: count,
        title: document.querySelector('#txt-title').value,
        body: document.querySelector('#txt-body').value
    });
    count++;

    showPosts();
    document.querySelector("#form-add-post").reset();
});

const showPosts = () => {
	//Create a varaible that will contain all the posts.
	let postEntries = "";


	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	});
	// To assign the value of postEntries to the element with "div-post-entries" id.
	document.querySelector("#div-post-entries").innerHTML = postEntries

};

const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;
};

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
    e.preventDefault();

    for(i = 0; i < posts.length; i++) {
        if (posts[i].id.toString() === document.querySelector('#txt-edit-id').value) {
            posts[i].title = document.querySelector('#txt-edit-title').value;
            posts[i].body = document.querySelector('#txt-edit-body').value;

            showPosts(posts);
            alert("Successfully updated!");
            break;
        }
    }
    document.querySelector("#form-edit-post").reset();
});

const deletePost = (id) => {
    for (i = 0; i < posts.length; i++) {
        console.log(posts[i].id);
        if (posts[i].id == id) {
            posts.splice(i, 1);
            console.log(posts);
            
            break;
        }
    }

    showPosts(posts);
}